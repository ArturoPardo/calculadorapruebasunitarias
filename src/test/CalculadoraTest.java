package test;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class CalculadoraTest {

	@Test(timeout = 100)
	//si una funcion tarda mas de ese tiempo se imprime el mensaje.
	public void testIntegerParseInt() {
		System.out.println("Hola");
	}

	@Test
	public void prueba1() {
		int resultado = Calculadora.multiplicarPor1(3);
		Assert.assertEquals(3, resultado);
	}

	@Test
	public void prueba2() {
		Assert.assertEquals(6, Calculadora.multiplicarPor2(3));
	}

	@BeforeClass
	//beforeclass te lo ejecuta al principio solo
	public static void pruebaBefore() {
		System.out.println("Hola, soy before");
	}
}
